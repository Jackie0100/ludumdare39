﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(cakeslice.Outline))]
public class InteractableBase : MonoBehaviour, IInteractable
{
    protected Generator generator;
    public Color GetTargetColor
    {
        get
        {
            return Color.red;
        }
    }

    public virtual float GetWattUsage
    {
        get
        {
            return 0;
        }
    }

    protected Generator GetGenerator
    {
        get
        {
            return generator;
        }
    }

    public virtual void OnInteract()
    {
    }

    public virtual void OnMouseOverItem()
    {
        if (this.enabled)
        {
            this.GetComponent<cakeslice.Outline>().enabled = true;
        }
    }

    // Use this for initialization
    protected virtual void Start ()
    {
        generator = GameObject.FindObjectOfType<Generator>();
        this.GetComponent<cakeslice.Outline>().enabled = false;
    }

    // Update is called once per frame
    protected virtual void Update ()
    {
		
	}
}

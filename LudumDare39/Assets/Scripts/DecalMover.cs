﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecalMover : MonoBehaviour
{
    [SerializeField]
    GameObject[] drawerdoors;
    [SerializeField]
    GameObject clueCanvas;
    [SerializeField]
    Text clueCanvasText;
    [SerializeField]
    Terminal targetTerminal;

    //18, 22
    // Use this for initialization
    void Start ()
    {
        targetTerminal.Passcode = Random.Range(1000, 9999);
        clueCanvasText.text = targetTerminal.Passcode.ToString();

        int targetdrawer = Random.Range(16, 32);

        if (targetdrawer % 7 <= 2)
        {
            targetdrawer += 2;
        }

        clueCanvas.transform.SetParent(drawerdoors[targetdrawer].transform.GetChild(1), false);

        this.transform.position = new Vector3(14.55f - Mathf.Floor(targetdrawer / 7.0f) * (3.8f / 4.0f), 0.025f, -3.75f - ((targetdrawer % 7) * (5.0f / 6.0f)));
    }
}

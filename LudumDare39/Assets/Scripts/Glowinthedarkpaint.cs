﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Glowinthedarkpaint : MonoBehaviour
{
    [SerializeField]
    AnimationCurve glowIncreaser;
    float glowCharge;
    [SerializeField]
    Text textComponent;

    private float GlowCharge
    {
        get
        {
            return glowCharge;
        }

        set
        {
            if (value >= 1)
            {
                glowCharge = 1;
            }
            else if (value <= 0)
            {
                glowCharge = 0;
            }
            else
            {
                glowCharge = value;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        if (!textComponent)
            textComponent = this.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (textComponent.enabled)
        {
            GlowCharge -= Time.deltaTime * 0.001f;
            textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, glowCharge);
        }
        else
        {
            GlowCharge += Time.deltaTime * glowIncreaser.Evaluate(glowCharge);
        }
    }

    public void ShowText(bool show)
    {
        textComponent.color = new Color(textComponent.color.r, textComponent.color.g, textComponent.color.b, glowCharge);

        textComponent.enabled = show;
    }
}

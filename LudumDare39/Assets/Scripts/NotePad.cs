﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotePad : InteractableBase
{
    [SerializeField]
    [Multiline]
    string notetext;
    [SerializeField]
    Terminal targetTerminal;
    [SerializeField]
    GameObject notecanvas;
    [SerializeField]
    Text notecanvastext;

    protected override void Start()
    {
        targetTerminal.Passcode = UnityEngine.Random.Range(1000, 9999);
        notetext += "The following equipment have been lost over the past few day, I've told the apprentice again and again that they're NOT allowed to remove any equipment, but do they listen? NOOOO " +
            "so yet again I need the stupid secretary to make a list over missing equipment...\n\nSo far I've found that the following have been lost...\n\n";
        notetext += GenerateText();
        notetext += "\n\nI'll have my secretary order this list alphabetically when it's done.";

        notecanvastext.text = notetext;
    }

    private string GenerateText()
    {
        string rettext = "";
        List<string> words = new List<string>() { "Bonesaws", "Chalk", "Cups", "Gloves", "Jars", "Scalps", "Scissors", "Trays" };
        Dictionary<string, int> selectedwords = new Dictionary<string, int>();
        //List <string> sortedselectedwords = new List<string>();

        int index = 0;
        for (int i = 0; i < 4; i++)
        {
            index = UnityEngine.Random.Range(0, words.Count);
            selectedwords.Add(words[index], 0);
            words.RemoveAt(index);
        }

        words = new List<string>() { "Bonesaws", "Chalk", "Cups", "Gloves", "Jars", "Scalps", "Scissors", "Trays" };

        index = 0;
        for (int i = 0; i < words.Count; i++)
        {
            if (selectedwords.ContainsKey(words[i].ToString()))
            {
                selectedwords[words[i]] = int.Parse(targetTerminal.Passcode.ToString()[index].ToString());
                index++;
            }
        }

        foreach (string key in selectedwords.Keys)
        {
            rettext += selectedwords[key].ToString() + " " + key.ToString() + "\n";
        }

        return rettext;
    }

    public override void OnInteract()
    {
        base.OnInteract();
        notecanvastext.text = notetext;

        notecanvas.SetActive(true);
    }

    public void CloseCanvas()
    {
        notecanvas.SetActive(false);
    }
}

﻿using UnityEngine;

public interface IInteractable
{
    void OnMouseOverItem();
    void OnInteract();
    Color GetTargetColor { get; }
    float GetWattUsage { get; }

}

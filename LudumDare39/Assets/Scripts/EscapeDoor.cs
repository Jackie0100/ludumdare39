﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EscapeDoor : InteractableBase
{
    int locksleft = 4;
    [SerializeField]
    AudioClip unlockdoorclip;
    [SerializeField]
    AudioClip opendoorclip;
    [SerializeField]
    GameObject gamecompletecanvas;
    [SerializeField]
    Text doorlocks;


    public override void OnInteract()
    {
        if (locksleft <= 0)
        {
            Debug.Log("Tester");
            this.GetComponent<AudioSource>().clip = opendoorclip;
            this.GetComponent<AudioSource>().Play();

            gamecompletecanvas.SetActive(true);
            GameObject.Find("Player").GetComponent<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController>().enabled = false;
        }
    }

    public void DecreaseCounter()
    {
        this.GetComponent<AudioSource>().clip = unlockdoorclip;
        this.GetComponent<AudioSource>().Play();
        locksleft--;
        doorlocks.text = locksleft.ToString();
    }
}

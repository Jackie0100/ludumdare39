﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Terminal : InteractableBase
{
    [SerializeField]
    int passcode;
    [SerializeField]
    Text passcodeWorldText;
    [SerializeField]
    Keypad keypadCanvas;
    [SerializeField]
    UnityEvent onCodeCorrect;

    public override float GetWattUsage
    {
        get
        {
            return 10;
        }
    }

    public int Passcode
    {
        get
        {
            return passcode;
        }

        set
        {
            passcode = value;
        }
    }

    public override void OnInteract()
    {
        base.OnInteract();
        GameObject.FindObjectOfType<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController>().enabled = false;
        keypadCanvas.targetterminal = this;
        keypadCanvas.gameObject.SetActive(true);
        GetGenerator.WattUsage += GetWattUsage;
    }

    public void SetupPassCode(int code)
    {
        if (passcodeWorldText)
        {
            passcodeWorldText.text = code.ToString();
        }
        passcode = code;
    }

    public void CheckCode(string input)
    {
        Debug.Log(passcode.ToString() == input);
        GameObject.FindObjectOfType<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController>().enabled = true;

        GetGenerator.WattUsage -= GetWattUsage;
        if (passcode.ToString() == input)
        {
            onCodeCorrect.Invoke();
            this.GetComponent<cakeslice.Outline>().eraseRenderer = true;

            this.enabled = false;
        }
    }
}

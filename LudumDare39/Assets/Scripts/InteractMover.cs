﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractMover : InteractableBase
{
    [SerializeField]
    GameObject[] enableOnInteract;

    public override void OnInteract()
    {
        base.OnInteract();
        this.transform.GetTopParent().position = new Vector3(0.35f, 1, 9.875f);
        this.transform.rotation = Quaternion.Euler(-5f, 180, 0);
        this.enabled = false;

        foreach (GameObject go in enableOnInteract)
        {
            go.SetActive(true);
        }
    }
}

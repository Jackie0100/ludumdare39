﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]
    float targetrotation;

    public void OpenDoor()
    {
        this.GetComponent<AudioSource>().Play();
        StartCoroutine(SwingDoor());
    }

    IEnumerator SwingDoor()
    {
        float timer = 0;
        Quaternion localrotation = this.transform.rotation;

        while (timer < 1)
        {
            timer += Time.deltaTime;
            this.transform.rotation = Quaternion.Lerp(localrotation, Quaternion.Euler(0, targetrotation, 0), timer);
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionClass
{
    public static T FindComponentInSiblings<T>(this Transform transform)
    {
        return transform.parent.GetComponentInChildren<T>();
    }

    public static T[] FindComponentsInSiblings<T>(this Transform transform)
    {
        return transform.parent.GetComponentsInChildren<T>();
    }

    public static Transform GetTopParent(this Transform transform)
    {
        if (transform.parent != null)
        {
            return GetTopParent(transform.parent);
        }
        return transform;
    }
}

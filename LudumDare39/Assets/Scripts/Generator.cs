﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    [Range(0, 10000)]
    [SerializeField]
    float powerLeft;
    [SerializeField]
    float wattUsage;
    [SerializeField]
    WattValueChangedEvent onWattValueChanged;
    [SerializeField]
    UnityEngine.Events.UnityEvent onWattDepleated;
    [SerializeField]
    Terminal[] passcodeTerminals;

    public float PowerLeft
    {
        get
        {
            return powerLeft;
        }

        set
        {
            powerLeft = value;

            if (powerLeft < 0)
            {
                powerLeft = 0;
                onWattDepleated.Invoke();
            }
            onWattValueChanged.Invoke(Mathf.Round(powerLeft).ToString());
        }
    }

    public float WattUsage
    {
        get
        {
            return wattUsage;
        }

        set
        {
            wattUsage = value;
        }
    }

    private void Start()
    {
        if (GammaCorrector.ambientlight != null)
        {
            RenderSettings.ambientLight = GammaCorrector.ambientlight;
        }
        foreach (Terminal t in passcodeTerminals)
        {
            if (t != null)
                t.SetupPassCode(Random.Range(1000, 9999));
        }

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update ()
    {
        PowerLeft -= (wattUsage * Time.deltaTime);
	}
}

[System.Serializable]
public class WattValueChangedEvent : UnityEngine.Events.UnityEvent<string>
{

}
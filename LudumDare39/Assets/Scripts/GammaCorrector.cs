﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GammaCorrector : MonoBehaviour
{
    public static Color ambientlight = new Color32(8, 8, 8, 8);

    public void GammaCorrection(float index)
    {
        RenderSettings.ambientLight = Color.Lerp(new Color32(4, 4, 4, 4), new Color32(128, 128, 128, 128), index);
        ambientlight = RenderSettings.ambientLight;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(0);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void StartGame()
    {
        SceneManager.LoadScene(1);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
}

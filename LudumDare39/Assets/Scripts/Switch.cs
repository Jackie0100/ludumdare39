﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : InteractableBase
{
    [SerializeField]
    GameObject lampLight;
    [SerializeField]
    Transform clicker;
    [SerializeField]
    Glowinthedarkpaint[] glowindarktext;

    public override float GetWattUsage
    {
        get
        {
            return 0.16666f;
        }
    }


    [SerializeField]
    [Tooltip("On when true.")]
    bool switcher;

	// Use this for initialization
	protected override void Start ()
    {
        base.Start();
        TurnOffLight();
	}

    // Update is called once per frame
    protected override void Update ()
    {
		
	}

    public override void OnInteract()
    {
        switcher = !switcher;
        this.GetComponent<AudioSource>().Play();
        if (switcher)
        {
            generator.WattUsage += GetWattUsage;
        }
        else
        {
            generator.WattUsage -= GetWattUsage;
        }
        TurnOffLight();
    }

    void TurnOffLight()
    {
        foreach (Light l in lampLight.GetComponentsInChildren<Light>())
        {
            l.enabled = switcher;
        }
        if (switcher)
        {
            clicker.rotation = Quaternion.Euler(0, 0, -20);
            lampLight.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
        }
        else
        {
            clicker.rotation = Quaternion.Euler(0, 0, 20);
            lampLight.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
        }
        foreach (Glowinthedarkpaint g in glowindarktext)
        {
            g.ShowText(!switcher);
        }
    }
}

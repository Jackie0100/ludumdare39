﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractController : MonoBehaviour
{
    cakeslice.OutlineEffect outlineEffectHandler;
    float lerptimer;
    [SerializeField]
    float lerpspeed = 1;

    float Lerptimer
    {
        get
        {
            return lerptimer;
        }
        set
        {
            if (value <= 0)
            {
                lerptimer = 0;
            }
            else if (value >= 1)
            {
                lerptimer = 1;
            }
            else
            {
                lerptimer = value;
            }
        }
    }
    Color targetcolor;
    Color oldcolor;
    [SerializeField]
    KeyCode interactKey = KeyCode.E;
    cakeslice.Outline targetoutline;

    // Use this for initialization
    void Start()
    {
        outlineEffectHandler = Camera.main.GetComponent<cakeslice.OutlineEffect>();
        outlineEffectHandler.lineColor0 = new Color(outlineEffectHandler.lineColor0.r, outlineEffectHandler.lineColor0.g, outlineEffectHandler.lineColor0.b, 0);
    }

    void Update ()
    {
        RaycastHit hitinfo;
        //TODO: Ray cast is a little of when standing in an angle.
        if (Physics.Raycast(Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0)), transform.TransformDirection(Vector3.forward), out hitinfo, 2, 0xFF))
        {
            Debug.Log(hitinfo.transform.name);
            if (hitinfo.transform.GetTopParent().GetComponentInChildren<InteractableBase>() != null && hitinfo.transform.GetTopParent().GetComponentInChildren<InteractableBase>().enabled)
            {
                InteractableBase targetinteractable = hitinfo.transform.GetTopParent().GetComponentInChildren<InteractableBase>();
                targetinteractable.OnMouseOverItem();
                if (Input.GetKeyDown(interactKey))
                {
                    targetinteractable.OnInteract();
                }

                targetcolor = targetinteractable.GetTargetColor;

                Lerptimer += Time.deltaTime * lerpspeed;
                outlineEffectHandler.lineColor0 = Color.Lerp(oldcolor, targetcolor, Lerptimer);
                if (targetoutline && targetinteractable.gameObject != targetoutline.gameObject)
                {
                    targetoutline.enabled = false;
                }
                targetoutline = targetinteractable.GetComponent<cakeslice.Outline>();
            }
            else
            {
                oldcolor = outlineEffectHandler.lineColor0;

                Lerptimer -= Time.deltaTime * lerpspeed;
                outlineEffectHandler.lineColor0 = Color.Lerp(new Color(targetcolor.r, targetcolor.g, targetcolor.b, 0), targetcolor, Lerptimer);
            }
        }
        else
        {
            oldcolor = outlineEffectHandler.lineColor0;

            Lerptimer -= Time.deltaTime * lerpspeed;
            outlineEffectHandler.lineColor0 = Color.Lerp(new Color(targetcolor.r, targetcolor.g, targetcolor.b, 0), targetcolor, Lerptimer);
        }
	}
}


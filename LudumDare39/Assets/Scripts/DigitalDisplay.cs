﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DigitalDisplay : MonoBehaviour
{
    [SerializeField]
    Text displayText;

    public void ChangeDisplayValue(string val)
    {
        val = val.Replace("1", " 1");
        displayText.text = val;
    }
}

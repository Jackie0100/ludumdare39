﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

internal class Keypad : MonoBehaviour
{
    string input;
    public Terminal targetterminal;

    public void InputClicked(string s)
    {
        if (s == "A")
        {
            targetterminal.CheckCode(input);
            input = "";
        }
        else if (s == "C")
        {
            input = "";
            targetterminal.CheckCode(input);
        }
        else
        {
            input += s;
        }
    }
}
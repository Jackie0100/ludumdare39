﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotepadDecal : InteractableBase
{
    [SerializeField]
    [Multiline]
    string notetext;
    [SerializeField]
    GameObject notecanvas;
    [SerializeField]
    Text notecanvastext;

    public override void OnInteract()
    {
        base.OnInteract();
        notecanvastext.text = notetext;

        notecanvas.SetActive(true);
    }

    public void CloseCanvas()
    {
        notecanvas.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorpseDrawersDoor : InteractableBase
{
    public override void OnInteract()
    {
        base.OnInteract();
        StartCoroutine(SwingDoor());
    }

    IEnumerator SwingDoor()
    {
        float timer = 0;
        Quaternion localrotation = this.transform.rotation;

        while (timer < 1)
        {
            Debug.Log(timer);

            timer += Time.deltaTime;
            this.transform.rotation = Quaternion.Lerp(localrotation, Quaternion.Euler(0, -180, 0), timer);
            yield return new WaitForEndOfFrame();
        }
        Debug.Log(timer);
        yield return null;
    }
}
